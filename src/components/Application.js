import React from 'react'

export default (props) => (
  <div className="img-with-text">
    <a href={props.onClick}><img src={props.src} alt={props.name} width="70px"/>
    <p className="applicationNames">{props.name}</p></a>
  </div>
)

import React from 'react';
import { Route } from 'react-router-dom'
import Home from './Home'
import About from './About'

const App = () => (
  <div>
    <header>
    <a href="#"><img src="../img/close.png" alt="close" /></a>
    </header>

    <main>
      <Route exact path="/" component={Home} />
      <Route exact path="/about-us" component={About} />
    </main>
  </div>
)

export default  App

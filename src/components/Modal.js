import React from 'react'
import Modal from 'react-modal'

import Application from './Application'
import {facebook, twitter} from '../config'


let appsShare = [
  {src: '../img/messages.png', name: 'Messages', onClick: 'sms:12345678'},
  {src: '../img/mail.png', name: 'Mail', onClick: 'mailto:name@rapidtables.com'},
  {src: '../img/reminders.png', name: 'Reminders', onClick: ''},
  {src: '../img/notes.png', name: 'Notes', onClick: ''},
  {src: '../img/twitter.png', name: 'Twitter', onClick: twitter},
  {src: '../img/facebook.png', name: 'Facebook', onClick: facebook},
  {src: '../img/flickr.png', name: 'Flickr', onClick: twitter}
]

let appsDefault = [
  {src: '../img/reads.png', name: 'Add to reading list'},
  {src: '../img/copy.png', name: 'Copy'},
  {src: '../img/else.png', name: 'More'}
]

const customStyles = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(255, 255, 255, 0)'
  },
  content: {
    top: '35%',
    left: '10px',
    right: '10px',
    bottom: '70px',
    margin: '0 auto',
    backgroundColor: "#edeeef",
    borderRadius: '15px',
    opacity: '0.95',
    maxWidth: '675px',
  }
}

const ModalComponent = props => (
  <div>
      <Modal
          isOpen={props.isOpen}
          style={customStyles}
          onRequestClose={props.close}
          contentLabel="IOS Applications"
          ariaHideApp={false}
        >
          <div className="modalRow airdrop">
            <img src="../img/airdrop.png" alt="Airdrop App" width="70px" />
            <p><b>Airdrop.</b> Tap to turn on Wi-Fi and Bluetooth to share with Airdrop.</p>

          </div>
          <div className="modalRow scrollRow">
            <hr/>
            <div className="horizontal-scroll-wrapper modalRow">
            {appsShare.map((app, i) => {
              return <Application key={i} onClick={app.onClick} src={app.src} name={app.name} />
            })}
            </div>
            <hr/>
          </div>
          <div className="modalRow">
            {appsDefault.map((app, i) => {
              return <Application key={i} src={app.src} name={app.name} />
            })}
          </div>
        </Modal>

      </div>
)

export default ModalComponent

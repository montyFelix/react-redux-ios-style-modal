import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { open, close } from '../reducers/modal'
import ModalComponent from './Modal'
import {link} from '../config'



const Home = props => (
  <div className="wrapper">
    <div className="envelope">
      <img id="envelopeImg" src="../img/envelope.png" alt="envelope" />
      <p>This is your personal invitation link:</p>
      <a className="link" onClick={() => props.changePage('/about-us')}>{link}</a>
      <p>For every friend who signs up, well move you upthe queue by at least 4000 places</p>
    </div>
    <div className="ButtonContainer">
      {props.isOpen ? <button id="closeModalButton" onClick={props.close}>Cancel</button> : <button onClick={props.open}>Share Invites</button>}
    </div>
    <ModalComponent isOpen={props.isOpen} close={props.close} />
  </div>
)

const mapStateToProps = state => ({
  isOpen: state.modal.isOpen,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  open,
  close,
  changePage: page => push(page)
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)

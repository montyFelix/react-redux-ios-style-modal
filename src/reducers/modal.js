export const OPEN_MODAL = 'modal/OPEN_MODAL'
export const CLOSE_MODAL = 'modal/CLOSE_MODAL'

const initialState = {
  isOpen: false
}

export default (state = initialState, action) => {
  switch(action.type) {
    case OPEN_MODAL:
      return {
        ...state,
        isOpen: true
      }

    case CLOSE_MODAL:
      return {
        ...state,
        isOpen: false
      }

    default:
      return state

  }
}

export const open = () => {
  return dispatch => {
    dispatch({
      type: OPEN_MODAL
    })
  }
}

export const close = () => {
  return dispatch => {
    dispatch({
      type: CLOSE_MODAL
    })
  }
}
